
function CowBoard() {
    
    this.WIDTH = 10;
    this.HEIGHT = 10;
    this.INNER_BORDER = 1;
    this.SIZE = 60 + 2 * this.INNER_BORDER;
    this.board = [];
    this.charactersOnBoard = [];
    this.user = {};
    this.treesOnBoard = [];
    
    this.htmlNode = null;
    
//    this.intervalFunc = null;    
    
    this.boardAttrs = {
        height: (this.SIZE * this.HEIGHT) + 'px',
        width: (this.SIZE * this.WIDTH) + 'px',
        position: 'relative',
        background: '#21c014',
        border: '1px solid black',
        margin: 'auto' // add 1 px          
    }
    
//    this.boardCell = {
//        'surface': null,
//        'unit': null
//    }
    
    this.generateBoard = function() {
        for (var y = 0; y < this.HEIGHT; y++) {
            var row = [];
            for (var x = 0; x < this.WIDTH; x++) {
//                row[x] = this.boardCell; // tak w klasie uzywam zawsze tego samego pola !!!
                   row[x] = {
                        'surface': null,
                        'unit': null
                    }
            }
            this.board.push(row);
        }        
    }
    
    this.initiateCharaters = function() {
        // add cows 
        var cows = [{x: 5, y: 5}, {x: 8, y: 1} , {x: 2, y: 5}, {x: 2, y: 2}]
        
        for (var i = 0; i < cows.length; i++) {
            var char1 = new Character('cow', cows[i]['x'], cows[i]['y'])
            this.charactersOnBoard.push(char1);            
            this.addCowToBoard(char1.posX, char1.posY);                  
        }
        
        // add oak trees 
        var trees = [{x: 7, y: 7}, {x: 6, y: 2} , {x: 3, y: 5}]
        
        for (var i = 0; i < trees.length; i++) {
            var tree1 = new Character('oak-tree', trees[i]['x'], trees[i]['y']);
            tree1.createBox = function () {
                this.box = document.createElement('div');
                this.box.setAttribute('style', this.createStyleAttr(this.attrs));
                $(this.box).append('<img src="oak-tree.png" style="width:'+this.WIDTH+'px;height:'+this.HEIGHT+'px" />');
            }   
            tree1.createBox();           
            this.treesOnBoard.push(tree1);
            this.addTreeToBoard(tree1.posX, tree1.posY);            
        }    
        
        // add iron trees 
        var trees = [{x: 2, y: 3}, {x: 3, y: 8}]
        
        for (var i = 0; i < trees.length; i++) {
            var tree1 = new Character('iron-tree', trees[i]['x'], trees[i]['y']);
            tree1.createBox = function () {
                this.box = document.createElement('div');
                this.box.setAttribute('style', this.createStyleAttr(this.attrs));
                $(this.box).append('<img src="iron-tree.png" style="width:'+this.WIDTH+'px;height:'+this.HEIGHT+'px" />');
            }   
            tree1.createBox();           
            this.treesOnBoard.push(tree1);
            this.addTreeToBoard(tree1.posX, tree1.posY);            
        }            
        
        
        // create user !!!
        
        this.user = new Character('user', 1, 1);
        
        // change object 
        this.user.createBox = function () {
            this.box = document.createElement('div');
            this.box.setAttribute('style', this.createStyleAttr(this.attrs));
            $(this.box).append('<img src="user.png" style="width:'+this.WIDTH+'px;height:'+this.HEIGHT+'px" />');
        }           
        
        this.user.createBox();        
        this.addUserToBoard(this.user.posX, this.user.posY);  
        
    }
    
    this.addCowToBoard = function(posX, posY) {
//        this.boardCell['unit'] = 'cow'; // to jest referencja !!!!
        this.board[posY][posX] = {surface: null,
                                unit: 'cow'};
    }

    this.addUserToBoard = function(posX, posY) {
        this.board[posY][posX] = {surface: null,
                                unit: 'user'};
        console.log('user added');
    }

    this.addTreeToBoard = function(posX, posY) {
        this.board[posY][posX] = {surface: 'oak-tree',
                                unit: null};
        console.log('oak-tree added');
    }
    
    this.createHtmlNode = function() {
        var body = document.getElementsByTagName('body');
        this.htmlNode = document.createElement('div');
        this.htmlNode.setAttribute('style', this.createStyleAttr(this.boardAttrs));
        this.htmlNode.intervalFunc = null;
        body[0].appendChild(this.htmlNode);        
    }
    
    this.displayBoard = function() {
        // display cows
        for (var i = 0; i < this.charactersOnBoard.length; i++) {            
            var char = this.charactersOnBoard[i].getBox();
            this.htmlNode.appendChild(char);
        }        
        
        // display user 
        this.htmlNode.appendChild(this.user.getBox());
        
        // display trees
        for (var i = 0; i < this.treesOnBoard.length; i++) {            
            var tree = this.treesOnBoard[i].getBox();
            this.htmlNode.appendChild(tree);
        }          
    }
    
    this.createStyleAttr = function (attrs) {
        var result = '';
        for (var key in attrs) {
            result += key + ':' + attrs[key] + ';';
        }
        return result;
    }    
    
    this.runGameLoop = function () {
        // bo to zadziala z punktu przegladarki dopiero a nie klasy teraz tu !!!!
        // bo ten kod sie wykonuje w przegladarce a nie na serwerze 
        console.log('loop');
//        console.log(this);
        for (var i = 0; i < this.charactersOnBoard.length; i++) {
//            console.log(this.charactersOnBoard[i]);
            this.charactersOnBoard[i].doAction(this.board);
        }
//        console.log(this.charactersOnBoard);
//        console.log(this.board);
    }
    
    
    this.main = function() {
        this.generateBoard();
        console.log(this.board);
        this.initiateCharaters();
        console.log(this.board);
        this.createHtmlNode();
        this.displayBoard();
//        this.htmlNode.intervalFunc = setInterval(this.runGameLoop, 500);
//        this.stopGameLoopListner();
    }
    
    this.main();
    
}