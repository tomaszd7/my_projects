var locations = [
    'www.kon.pl',
    'www.kon.com.pl',
    'www.kon.de',
    'www.kon.com',
    'www.kon.eu',
    'www.kon.ch'
];


document.addEventListener('DOMContentLoaded', function () {

    document.myError = {};
    locations.forEach(function (value, key) {
        var url = "http://" + value + "/";

        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState == 4) {
                var p = document.createElement('p');
                var section = document.getElementById('section');
                p.textContent = this.status + '\t' + url;
                section.appendChild(p);
                document.myError[url] = this;
            }
        };
        xhttp.open("HEAD", url, true);
        xhttp.send();

    })



});