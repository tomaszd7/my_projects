
//wtyczka - skrypt ktory mozna na czyms uruchomic 

(function ($) {

    $.fn.showLinkLocation = function () {

        this.filter("a").each(function () {
            var link = $(this);
            link.append(" (" + link.attr("href") + ")");
        });

        return this;

    };
}(jQuery));


(function ($) {

    $.fn.myWidget = function () {

//        this.mouseenter(function (event) {
        $(this).click(function (event) {


            var $item = $(this).clone();
            var smallHeight = $(this).height();
            $item.addClass('full-size');
            var xOffset = event.clientX - this.naturalWidth / 2;
            var yOffset = (smallHeight) * 0.1;
            $item.css({'left': xOffset, 'top': yOffset});
            $item.css('transform', 'scale(1)');
            $(this).after($item);


            $item.mousemove(function (event) {
                if (typeof window.myY === 'undefined') {
                    window.myY = event.clientY;
                }
                var xOffset = event.clientX - this.naturalWidth / 2;
                var yOffset = event.clientY - window.myY;
                if (yOffset === 0) {
                    $(this).css('transform', 'scale(1)');
                } else {
                    $(this).css('transform', 'scale(' + (1 + Math.abs(yOffset / 100)) + ')');
                }
                $(this).css({'left': xOffset});
                console.log($(this).css('transform'));
                event.stopPropagation();
            })

            $item.click(function () {
                $(this).remove();
                delete window.myY;
                event.stopPropagation();
            })
        })
//        });

        return this;
    };
}(jQuery));
