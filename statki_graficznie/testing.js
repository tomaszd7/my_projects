

// var obiekt = {
// 	dodajTeksty: function (tekst, drugiTekst) { // czyli parametr to jest to co jest 
// 		// potrzebne z zewnatrz ... nie musi brac z siebie - obiektu 
// 		this.tekst = tekst;
// 		this.drugiTekst = drugiTekst;
// 		console.log(this.tekst + ' ' + this.drugiTekst);
// 	},
// 	tekst: '',
// 	drugiTekst: ''
// }


// obiekt.tekst = 'mama';
// obiekt.drugiTekst = 'tata';
// obiekt.dodajTeksty('ja', 'ty');

// if (!Array.prototype.forEach)
// {
//   Array.prototype.forEach = function(fun /*, thisp*/)
//   {
//     var len = this.length;
//     if (typeof fun != "function")
//       throw new TypeError();

//     var thisp = arguments[1];
//     for (var i = 0; i < len; i++)
//     {
//       if (i in this)
//         fun.call(thisp, this[i], i, this);
//     }
//   };
// }

// var text = 'To jest moj tekst';
// var myArray = [4, 6, 2, 5];
// myArray.forEach(
// 	function(element) 
// 	{
// 		console.log(element);
// 	}

// )

// for (var element in myArray) {  // tu nie bierze kolejnosci !!!
// 	console.log(myArray[element]);
// }






function runGame() {
	// get all tds but not the first with names 
	// does NOT take GRID into account 
	function addEventToAllCells(activate = true) {
		var allCells = document.getElementsByTagName("td");
		for (var index in allCells) {
			if( !(index == 0 || index % 11 == 0)) {
				if (!activate) {
					// stop the game 
					allCells[index].onclick = null;
					gameStart.style.backgroundColor = '#263069';
					gameStart.style.color = '#white';

				} else {
					// start the game 
					// change button color 
					gameStart.style.backgroundColor = 'buttonface';
					gameStart.style.color = 'white';
					// add Event handler to all table cells 
					allCells[index].onclick = function() {
						if (this.classList.contains('black')) {
							this.classList.remove('black');
						} else {
							this.classList.add('black');
						}
					}
					// remove black classes  - add to run after first time !!
					// to clear the board!!
					removeBlackClass();
				}
			}
		}
	}


	function getRandomCell() {
		// losowanie randomowego elementu tablicy 
		var goodSpot = false;
		do {
			// check if good spot - so not already guessed! 
			var randomX = Math.floor((Math.random()* GRID) + 1);
			var randomY = Math.floor((Math.random()* GRID) + 1);

			var cellId = randomX + '-' + randomY;
			if (!document.getElementById(cellId).classList.contains('black')) {
				goodSpot = true;
			} else {
				console.log('DUPLIKAT!!!' + cellId);
			}
		} while (!goodSpot)

		console.log(cellId);

		// zapalenie wylosowanego elementu tabeli
		document.getElementById(cellId).classList.add('black');
	}


	function removeBlackClass() {
		var itemsToRemove = document.getElementsByClassName("black"); // to zwraca po indeksach i po id!!
		// a ja musze iterowac tylko po indeksach !!  nieee - tylko tak wystwietla  ?? 
		if (itemsToRemove.length !== 0){
			for (var i = 0; i < itemsToRemove.length; i++) { // to pojdzie tezpo associated table a nietylko indeksach 
				// jak zwykly for !!! bo tu wzial pole .length !!! i polecial po ni m shit !!!!! 
				// wieczwyklyfor musi byc !! -czyli po indeksach 
				itemsToRemove[i].classList.remove('black');
			}
		}
	}

	function hideElements(GRID) {
		// get theader row
		var thead = document.getElementsByTagName('thead');
		var headRows = thead[0].getElementsByTagName('tr');
		var headCells = headRows[0].getElementsByTagName('th');

		// get rows 
		var tbody = document.getElementsByTagName('tbody');
		var bodyRows = tbody[0].getElementsByTagName('tr');

		for (var i = GRID ; i < bodyRows.length; i++) { // od 5 do 10 
			// hide rows 
			bodyRows[i].style.display = 'none';
			//hide cells in thead 
			headCells[i + 1].style.display = 'none';

			// hide cells in tbody
			for (var j = 0 ; j < GRID; j ++) {
				var bodyCells = bodyRows[j].getElementsByTagName('td');
				bodyCells[i + 1].style.display = 'none';
			}
		}
	}

	// czyli pyta sie type of !== undefined !!! a nie obiekt !! -czy pola istnieja 


	/*KOD GAME STARTS HERE*/

	console.log('NEW GAME STARTS HERE');
	// get gridsize from field button 
	var GRID;
	var sel = document.getElementsByTagName('select');
	var opts = sel[0].getElementsByTagName('option')
	for (var i = 0; i < opts.length; i++) {
		if (opts[i].selected) {
			GRID = parseInt(opts[i].value);
		}
	}

	// must ADD unhide all elements !!! 

	// activate only some cells 
	if (GRID !== 10) {
		hideElements(GRID);
	}

	// add GRID to results window
	document.getElementById("resGridSize").innerText = GRID;

	// add Event handler to td cells for adding black class 
	addEventToAllCells();

	// start timer and random coloring 
	var start = new Date();
	start = start.getTime();
	console.log(start);
	var end;

	// glowna petla programu  - to nie moze byc w do!!!!
	// srpbujmy for 
	for (var i = 0; i < 5000/500; i++) {
	// do {
		setTimeout(getRandomCell, 1);// i tak masz randomowe 10 elementow :)))) 
		// end = new Date();
		// end = end.getTime();

	// } while (end - start < 5000)
	}

	// now with setInterval - czy musi byc w tej funkcji by sie zatrzymac??? 


	// end of game - TIMES OUT!
	console.log('finished');

	// clearing variables 
	// clearInterval(intervalVar)
	addEventToAllCells(false);
}

var gameStart = document.getElementById("button");
gameStart.onclick = runGame;


// create table cells with ids!!!

// function createTable() {
// 	var myData = '';
// 	for (var i = 1; i <= 10; i++) {
// 		myData+='<tr>\n';
// 		for (var j = 0; j <=10; j++) {
// 			if (j == 0) {
// 				myData+='<td>'+ i + '</td>\n';
// 			} else {
// 				myData+='<td id=\'' + j + '-' + i + '\'></td>\n';
// 			}
// 		}
// 		myData+='</tr>\n';
// 	}
// 	return myData;
// }

// document.getElementById("paragraph").innerText = createTable();