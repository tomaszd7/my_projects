'use strict';

function generate(x) {
	var board = [];
	for (var i = 0; i < x; i++) {// to jest y 
        board[i] = [];
        for (var j = 0; j < x; j++) {
            board[i][j] = 0;
//            board[i].push('ala');   - to zadziala tak samo 
            }
    }
    return board;
}

function isGoodSpot(board, x, y) {
    // szuka jedynki - wtedy false - zle miejsce 
    // domyslnie jest true - dobre miejsce 
    
    // sposob nadpisanie x i y dobrymi parametrami 
    // to podejscie jest lepsze bo bedzie mniej iteracji robilo !!
//    var startX = x - 1;
//
//    if (startX < 0) {
//        startX = 0;
//    }    
//    var endX = x + 1;
//    
//    if (endX >= board.length) {
//        endX = board.length - 1;
//    }    
//    var startY = y - 1;
//    
//    if (startY < 0) {
//        startY = 0;
//    }    
//    var endY = y + 1;
    
    for (var i = x - 1; i <= x + 1; i++) {
        if (i >= 0 && i < board.length) {
            for (var j = y - 1; j <= y + 1; j++) {
                if (j >= 0 && j < board.length) {
                    if (board[i][j] === 1 ) {
                        return false;
                    }
                }
            }
        }
    }
    
    return true;
    
}


function randomShip(board, ships) {
    //for po statkach 
//    for (var i = 0; i < ships; i++) {
//        var x = Math.round(Math.random() * board.length);
//        var y = Math.round(Math.random() * board.length);
//        if (board[x][y] === 0) {
//            board[x][y] = 1;
//        } else {
//            i--; // fajny pomysl po prostu zmienjszy i
//        }
//        // mozna whilem az zrobil 3 statki 
//        // lub funkcje rekurencyjnie s
//    }
    // to samo na do 
    do {
        var x = Math.floor(Math.random() * board.length);
        var y = Math.floor(Math.random() * board.length);
        if (isGoodSpot(board,x, y)) { // tu zmiana dla sprawdzania sasiadow 
            board[x][y] = 1;
            ships--;
        }
    } while (ships > 0)
        
    // rekurencyjnie bazujac na for 
//    for (var i = 0; i < ships; i++) {
//        function getRandom() {
//            var x = Math.floor(Math.random() * board.length);
//            var y = Math.floor(Math.random() * board.length);
//            if (board[x][y] === 0) {
//                board[x][y] = 1;
//                return;
//            } else {getRandom();}
//        }   
//        getRandom();
//    }
    return board; 
    // wprawdzie JS  nadpisze ta tablice w funkcji 
    // ale dla dobrego kodu zwracamy ta tablice 
    //proste typy nie zmieni ...  w PHP zdefiniujemy czy ma zmienic zmienna wchodzaca czy zrobic jej kopie !!
}



function hit(board, x, y) {
    board[x][y] = 2;
    return board; // to jest ladna konwencja choc nie musi byc w JS
}

function writeBoard(board) {
    // ale od uzytkownika tez przypisujemy odwortnie !!! najpierw y pozniej x 
    document.write('X: ');
    for (var z = 0; z < board.length; z++){
        document.write(z);
    }
    document.write('<br/>')
    for (var i = 0; i < board.length; i++) {
        document.write(i + ': ');
        for (var j = 0; j < board.length; j++) {
            document.write(board[i][j]);
        }
        document.write('<br/>')
    }
    
}

function canon(board, x, y) { // tenazwy zmiennych maja miec sens 
    // w kontekscie srodka tej funkcji 
    if (board[x][y] === 1) {
        console.log('Trafiony');
        return 'Trafiony';
    } else {
        console.log('Pudło');
        return 'Pudło';
    }
    //przy 2 wymiarowej blad bedzie nie maproperty of undefined 
    // bo pierwszy indeks zly da undefined !!! - dlatego trzeba sprawdzac wszystkie?? jak z tymi wezlami ??
}

// dodaj petle do ciaglego strzelania az skoncza sie statki 
function getProperNum(board, text) {
//    var number = null;
    do {
        var num = prompt(text);
//        temp = parseInt(temp);
//        if (temp >= 0 && temp < board.length) {
//            number = temp;
//        }
//    } while (number === null);
//    } while(!(num >= 0 && num < board.length)) // mozna bez not !!
    } while (num < 0 || num >= board.length || isNaN(num) || num === null) 
        /*- tu bez negacji z OR !!! bo sprawdzamy warunki by byl w petli */
        // isNan - juz rzutuje na  liczbe !
        // null - jest gdy wcisnie sie Anuluj 
        //dowhile jak dlugowarunek jest prawdziwy 
    return num;
}
    // parseInt('tekst') zwroci NaN ... a liczba zwroci liczbe 



// KOD 
// generuj pusta plansze 
var myBoard = generate(3);

// dodaj statki 
var shipsOnBoard = 1;
myBoard = randomShip(myBoard,shipsOnBoard); // tu opis wyzej o czystym kodzie 

writeBoard(myBoard);

var startTime = new Date();
var start = startTime.getTime(); // lepiej tak??

do {
    // pytaj usera o strzal 
    
    var a = getProperNum(myBoard, 'Zmienna X: ' );
    var b = getProperNum(myBoard, 'Zmienna Y: ' );
//    var b = prompt('Zmienna Y: ');
//    console.log(myBoard);

    // uzupelnij plansze po strzale 
    if (canon(myBoard, a, b) === 'Trafiony') {
        myBoard = hit(myBoard, a, b); // oznacz jako 2 nie  dziala !!
        shipsOnBoard--;
        writeBoard(myBoard);
 
    }
//    console.log(myBoard);
} while (shipsOnBoard > 0);
    
var endTime = new Date();
var end =endTime.getTime();

var timeDiff = end - start;
console.log(Math.round(timeDiff/100) / 10); //to by zrobil zaokraglenie do 1/10?
