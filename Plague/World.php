<?php


/**
 * Description of World
 *
 * @author Python
 */
class World {
    //put your code here
    private $population;
    private $infected;
    private $virus;
    
    public function __construct($worldData, Virus $virus) {
        $this->virus = $virus;
        $this->population = $worldData['population'];
        $this->infected = $worldData['infected'];        
    }
    
    public function virusTurn() {
        $virusData = $this->virus->getVirusData();
        // add spreading 
        $this->infected += $virusData['spreading'];
        // add killing
        $this->population -= $virusData['lethality'];
        // make older 
        $this->virus->makeOlder();
        print_r($virusData);
    }
    
    public function getWorldData() {
        return [
            'population' => $this->population,
            'infected' => $this->infected
        ];
    }
}
