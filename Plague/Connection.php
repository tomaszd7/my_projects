<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Connection
 *
 * @author Python
 */
class Connection {

    private $worldData = [
        'population' => 1000,
        'infected' => 1
    ];
    
    private $virusData = [
        'spreading' => null,
        'lethality' => null,
        'age' => null        
    ];
    
    public function __construct() {
        $this->getUserData();
    }
    
    private function getUserData() {
        
        if (isset($_POST['population'])) {
            $this->gameData['population'] = $_POST['population'];
            $this->gameData['infected'] = $_POST['infected'];
            // $this->updateCookies();
        } else {
            // read cookies 
        }
    }

    public function updateCookies() {

    }

    public function getGameData() {
        return [$this->worldData, $this->virusData];
    }
}
