<?php

/**
 * Description of Virus
 *
 * @author Python
 */
class Virus {
 
    private $spreading;
    private $lethality;
    private $age = 0;
    private $ageToMutate = 3;
    private $mutations = [
        [ 'spreading' => 1, 'lethality' => 1],
        [ 'spreading' => 2, 'lethality' => 0],
        [ 'spreading' => 0, 'lethality' => 1]        
    ] ;
    
    public function __construct($virusData) {
        // przypisanie zmiennych 
        if ($virusData['spreading'] === null) {
        $this->pickMutation();
        } else {
            $this->spreading = $virusData['spreading'];
            $this->lethality = $virusData['lethality'];
            $this->age = $virusData['age'];
        }
    }
    
    private function pickMutation() {
        // moze wylosawac ta ktora juz jest 
        $key = array_rand($this->mutations);
        $this->spreading = $this->mutations[$key]['spreading'];
        $this->lethality = $this->mutations[$key]['lethality'];             
    }
    
    private function isReadyToMutate() {
        if ($this->age >= $this->ageToMutate) {
            $this->age = 0;
            return true;
        }
        return false;
    }
    
    public function makeOlder() {
        $this->age++;
        // mutate if ready
        if ($this->isReadyToMutate()) {
            $this->pickMutation();          
        }        
    }
    
    public function getVirusData() {
        return [ 'spreading' => $this->spreading,
                 'lethality' => $this->lethality,
                 'age' => $this->age
        ];            
    }

}
